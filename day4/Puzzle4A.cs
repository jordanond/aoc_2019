using System;
using System.Collections.Generic;
using System.Linq;

namespace aoc_2019.day4
{
    public class Puzzle4A
    {
        public Puzzle4A() {
            var results = BruteForceRange(367479, 893698);
            Console.WriteLine($"Sample Matches:");
            foreach (var result in results.Take(100)) {
                Console.WriteLine(result);
            }
            Console.WriteLine($"Total Count: {results.Count()}");
        }

        private IEnumerable<int> BruteForceRange(int from, int to) {
            for (int i = from; i < to; i++) {
                if (!IsSixDigits(i)) {
                    continue;
                }

                var digits = GetDigits(i);
                if (NeverDecreases(digits) && HasDoubleDigits(digits)) {
                    yield return i;
                }
            }
        }

        private bool IsSixDigits(int value) {
            return value >= 100000 && value < 1000000;
        }

        private int[] GetDigits(int value) {
            int[] digits = new int[6];
            int mod = 10;
            for (int i = 5; i >= 0; i--) {
                digits[i] = (value % mod) / (mod / 10);
                mod *= 10;
            }
            return digits;
        }

        private bool NeverDecreases(int[] digits) {
            for (int i = 0; i < digits.Length - 1; i++) {
                var minValue = digits[i];
                if (digits[i + 1] < minValue) {
                    return false;
                }
            }
            return true;
        }

        private bool HasDoubleDigits(int[] digits) {
            for (int i = 0; i < digits.Length - 1; i++) {
                if (digits[i] == digits[i + 1]) {
                    return true;
                }
            }
            return false;
        }
    }
}