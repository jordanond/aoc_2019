using System;
using System.Collections.Generic;
using System.Linq;

namespace aoc_2019.day4
{
    public class Puzzle4B
    {
        public Puzzle4B() {
            var results = BruteForceRange(367479, 893698);
            Console.WriteLine($"Matches:");
            foreach (var result in results) {
                Console.WriteLine(result);
            }
            Console.WriteLine($"Total Count: {results.Count()}");
        }

        private IEnumerable<int> BruteForceRange(int from, int to) {
            for (int i = from; i < to; i++) {
                if (!IsSixDigits(i)) {
                    continue;
                }

                var digits = GetDigits(i);
                if (NeverDecreases(digits) && HasDoubleDigitsOnly(digits)) {
                    yield return i;
                }
            }
        }

        private bool IsSixDigits(int value) {
            return value >= 100000 && value < 1000000;
        }

        private int[] GetDigits(int value) {
            int[] digits = new int[6];
            int mod = 10;
            for (int i = 5; i >= 0; i--) {
                digits[i] = (value % mod) / (mod / 10);
                mod *= 10;
            }
            return digits;
        }

        private bool NeverDecreases(int[] digits) {
            for (int i = 0; i < digits.Length - 1; i++) {
                var minValue = digits[i];
                if (digits[i + 1] < minValue) {
                    return false;
                }
            }
            return true;
        }

        private bool HasDoubleDigitsOnly(int[] digits) {
            var duplicateCount = new List<int>();
            var repeatCount = 0;
            var repeatedDigit = -1;
            for (int i = 0; i < digits.Length - 1; i++) {
                if (digits[i] == digits[i + 1]) {
                    if (repeatedDigit == digits[i]) {
                        repeatCount++;
                    } else {
                        if (repeatCount > 0) {
                            duplicateCount.Add(repeatCount);
                        }
                        repeatCount = 2;
                    }
                    repeatedDigit = digits[i];
                }
            }
            if (repeatCount > 0) {
                duplicateCount.Add(repeatCount);
            }
            return duplicateCount.Any(x => x == 2);
        }
    }
}