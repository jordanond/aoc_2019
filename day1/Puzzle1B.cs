using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace aoc_2019.day1
{
    public class Puzzle1B
    {
        public Puzzle1B() {
            var masses = File.ReadAllLines("day1/1a.input").Select(line => int.Parse(line));
            Console.WriteLine($"Total Fuel Usage: {GetTotalFuelUsage(masses)}");
        }

        private int GetTotalFuelUsage(IEnumerable<int> masses) {
            return masses.Select(mass => GetTotalFuelForModule(mass)).Sum();
        }

        private int GetTotalFuelForModule(int mass) {
            int totalFuelRequired = 0;
            int fuelRequiredForStep = GetFuelForStep(mass);

            while (fuelRequiredForStep >= 0) {
                totalFuelRequired += fuelRequiredForStep;
                fuelRequiredForStep = GetFuelForStep(fuelRequiredForStep);
            }

            return totalFuelRequired;
        }

        private int GetFuelForStep(int mass) {
            return (int)(mass / 3) - 2;
        }
    }
}