using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace aoc_2019.day1
{
    public class Puzzle1A
    {
        public Puzzle1A() {
            var masses = File.ReadAllLines("day1/1a.input").Select(line => int.Parse(line));
            Console.WriteLine($"Total Fuel Usage: {GetTotalFuelUsage(masses)}");
        }

        private int GetTotalFuelUsage(IEnumerable<int> masses) {
            return masses.Select(mass => GetFuelForModule(mass)).Sum();
        }

        private int GetFuelForModule(int mass) {
            return (int)(mass / 3) - 2;
        }
    }
}