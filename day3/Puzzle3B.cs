using System;
using System.IO;
using System.Linq;
using aoc_2019.extensions;

namespace aoc_2019.day3
{
    public class Puzzle3B
    {
        public Puzzle3B() {
            var instructions = File.ReadAllLines("day3/3a.input").Select(wire => wire.Split(',')).ToArray();
            for (int i = 0; i < instructions.Length; i += 2) {
                Console.WriteLine($"Run #{(i / 2) + 1}");
                var wireA = new Wire(instructions[i]);
                var wireB = new Wire(instructions[i + 1]);
                wireA.GetIntersectionsWithTotalDistance(wireB).OrderBy(x => x).Skip(1).Take(1).ForEach(x => {
                    Console.WriteLine(x);
                });
            }
        }
    }
}