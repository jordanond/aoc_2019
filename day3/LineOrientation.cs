namespace aoc_2019.day3
{
    public enum LineOrientation
    {
        Horizontal,
        Vertical
    }
}