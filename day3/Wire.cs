using System;
using System.Collections.Generic;

namespace aoc_2019.day3
{
    public class Wire
    {
        public List<Line> Lines { get; set; }

        public Wire(string[] instructions) {
            this.Lines = new List<Line>();
            Point currentPoint = new Point(0, 0);
            for (int i = 0; i < instructions.Length; i++) {
                currentPoint = BuildLine(currentPoint, instructions[i]);
            }
        }

        private Point BuildLine(Point p, string instruction) {
            var dist = int.Parse(instruction.Substring(1));
            switch (instruction[0]) {
                case 'U':
                    this.Lines.Add(new Line { A = new Point(p.X, p.Y), B = new Point(p.X, p.Y + dist), Direction = LineDirection.Up });
                    return new Point(p.X, p.Y + dist);
                case 'R':
                    this.Lines.Add(new Line { A = new Point(p.X, p.Y), B = new Point(p.X + dist, p.Y), Direction = LineDirection.Right });
                    return new Point(p.X + dist, p.Y);
                case 'D':
                    this.Lines.Add(new Line { A = new Point(p.X, p.Y), B = new Point(p.X, p.Y - dist), Direction = LineDirection.Down });
                    return new Point(p.X, p.Y - dist);
                case 'L':
                    this.Lines.Add(new Line { A = new Point(p.X, p.Y), B = new Point(p.X - dist, p.Y), Direction = LineDirection.Left });
                    return new Point(p.X - dist, p.Y);
                default:
                    return null;
            }
        }

        public IEnumerable<Point> GetIntersections(Wire other) {
            for (int i = 0; i < this.Lines.Count; i++) {
                for (int j = 0; j < other.Lines.Count; j++) {
                    var intersectingPoint = this.Lines[i].GetIntersectingPoint(other.Lines[j]);
                    if (intersectingPoint != null) {
                        yield return intersectingPoint;
                    }
                }
            }
        }

        public IEnumerable<int> GetIntersectionsWithTotalDistance(Wire other) {
            var runningWireATotal = 0;
            var runningWireBTotal = 0;
            for (int i = 0; i < this.Lines.Count; i++) {
                for (int j = 0; j < other.Lines.Count; j++) {
                    if (j == 0) {
                        runningWireBTotal = 0;
                    }

                    var intersectingPoint = this.Lines[i].GetIntersectingPoint(other.Lines[j]);
                    if (intersectingPoint != null) {
                        var intersectionDistance = 
                            GetDirectionalDistanceToPoint(this.Lines[i], intersectingPoint) 
                            + GetDirectionalDistanceToPoint(other.Lines[j], intersectingPoint);

                        yield return (runningWireATotal + runningWireBTotal + intersectionDistance);
                    }
                    runningWireBTotal += other.Lines[j].Length;
                }
                runningWireATotal += this.Lines[i].Length;
            }
        }

        public int GetDirectionalDistanceToPoint(Line line, Point point) {
            switch (line.Direction) {
                case LineDirection.Up:
                    return point.Y - line.A.Y;
                case LineDirection.Right:
                    return point.X - line.A.X;
                case LineDirection.Down:
                    return line.A.Y - point.Y;
                case LineDirection.Left:
                    return line.A.X - point.X;
                default:
                    return 0;
            }
        }
    }
}