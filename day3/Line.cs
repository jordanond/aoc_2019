using System;

namespace aoc_2019.day3
{
    public class Line
    {
        public Point A { get; set; }

        public Point B { get; set; }

        public int Length => Math.Abs(A.X - B.X) + Math.Abs(A.Y - B.Y);

        public LineOrientation Orientation => this.A.X == this.B.X ? LineOrientation.Vertical : LineOrientation.Horizontal;

        public LineDirection Direction { get; set; }

        public Point GetIntersectingPoint(Line other) {
            if (this.Orientation == other.Orientation) {
                return null;
            }

            if (this.Orientation == LineOrientation.Horizontal) {
                var xMin = Math.Min(this.A.X, this.B.X);
                var xMax = Math.Max(this.A.X, this.B.X);
                var oyMin = Math.Min(other.A.Y, other.B.Y);
                var oyMax = Math.Max(other.A.Y, other.B.Y);
                if (xMin <= other.A.X && xMax >= other.A.X && oyMin <= this.A.Y && oyMax >= this.A.Y) {
                    return new Point(other.A.X, this.A.Y);
                }
            } else {
                var yMin = Math.Min(this.A.Y, this.B.Y);
                var yMax = Math.Max(this.A.Y, this.B.Y);
                var oxMin = Math.Min(other.A.X, other.B.X);
                var oxMax = Math.Max(other.A.X, other.B.X);
                if (yMin <= other.A.Y && yMax >= other.A.Y && oxMin <= this.A.X && oxMax >= this.A.X) {
                    return new Point(this.A.X, other.A.Y);
                }
            }
            return null;
        }
    }
}