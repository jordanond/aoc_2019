using System;
using System.IO;
using System.Linq;
using aoc_2019.extensions;

namespace aoc_2019.day3
{
    public class Puzzle3A
    {
        public Puzzle3A() {
            var instructions = File.ReadAllLines("day3/3a.input").Select(wire => wire.Split(',')).ToArray();
            for (int i = 0; i < instructions.Length; i += 2) {
                Console.WriteLine($"Run #{(i / 2) + 1}");
                var wireA = new Wire(instructions[i]);
                var wireB = new Wire(instructions[i + 1]);
                wireA.GetIntersections(wireB).Skip(1).OrderBy(a => Math.Abs(a.X) + Math.Abs(a.Y)).ForEach(x => {
                    Console.WriteLine($"({x.X}, {x.Y}) -> {Math.Abs(x.X) + Math.Abs(x.Y)}");
                });
            }
        }
    }
}