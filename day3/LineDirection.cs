namespace aoc_2019.day3
{
    public enum LineDirection
    {
        Up,
        Right,
        Down,
        Left
    }
}