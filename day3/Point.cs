using System;

namespace aoc_2019.day3
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y) {
            this.X = x;
            this.Y = y;
        }
    }
}