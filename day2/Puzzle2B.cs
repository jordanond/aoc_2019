using System;
using System.IO;
using System.Linq;

namespace aoc_2019.day2
{
    public class Puzzle2B
    {
        public Puzzle2B() {
            for (int noun = 0; noun < 100; noun++) {
                for (int verb = 0; verb < 100; verb++) {
                    var inputs = File.ReadAllText("day2/2a.input").Split(',').Select(input => int.Parse(input)).ToArray();
                    inputs[1] = noun;
                    inputs[2] = verb;

                    if (CalculateInput(inputs) == 19690720) {
                        Console.WriteLine($"noun={noun}; verb={verb};");
                    }
                }
            }
            
        }

        private int CalculateInput(int[] inputs) {
            for (int i = 0; i < inputs.Length; i += 4) {
                switch (inputs[i]) {
                    case 1:
                        inputs[inputs[i + 3]] = inputs[inputs[i + 1]] + inputs[inputs[i + 2]];
                        break;
                    case 2:
                        inputs[inputs[i + 3]] = inputs[inputs[i + 1]] * inputs[inputs[i + 2]];
                        break;
                    case 99:
                        return inputs[0];
                    default:
                        return -1;
                }
            }

            return -1;
        }
    }
}