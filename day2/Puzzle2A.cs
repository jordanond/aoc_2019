using System;
using System.IO;
using System.Linq;

namespace aoc_2019.day2
{
    public class Puzzle2A
    {
        public Puzzle2A() {
            var inputs = File.ReadAllText("day2/2a.input").Split(',').Select(input => int.Parse(input)).ToArray();
            // var inputs = "1,9,10,3,2,3,11,0,99,30,40,50".Split(',').Select(input => int.Parse(input)).ToArray();
            inputs[1] = 12;
            inputs[2] = 2;
            Console.WriteLine($"opcodes: {String.Join(',', inputs)}");
            CalculateInput(inputs);
            Console.WriteLine($"opcodes: {String.Join(',', inputs)}");
        }

        private void CalculateInput(int[] inputs) {
            for (int i = 0; i < inputs.Length; i += 4) {
                switch (inputs[i]) {
                    case 1:
                        Console.WriteLine($"[{inputs[inputs[i + 3]]}] = {inputs[inputs[i + 1]]} + {inputs[inputs[i + 2]]}");
                        inputs[inputs[i + 3]] = inputs[inputs[i + 1]] + inputs[inputs[i + 2]];
                        break;
                    case 2:
                        Console.WriteLine($"[{inputs[inputs[i + 3]]}] = {inputs[inputs[i + 1]]} * {inputs[inputs[i + 2]]}");
                        inputs[inputs[i + 3]] = inputs[inputs[i + 1]] * inputs[inputs[i + 2]];
                        break;
                    case 99:
                        Console.WriteLine($"Exiting program...");
                        return;
                    default:
                        Console.WriteLine($"Invalid opcode: {inputs[i]}");
                        break;
                }
            }
        }
    }
}