using System;
using System.Collections.Generic;

namespace aoc_2019.extensions
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {   // note: omitted arg/null checks
            foreach(T item in source) { action(item); }
        }
    }
}